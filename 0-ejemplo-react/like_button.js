'use strict'

const e = React.createElement

// define un componente de React llamado LikeButton
class LikeButton extends React.Component {
  constructor (props) {
    super(props)
    this.state = { liked: false }
  }

  render () {
    if (this.state.liked) {
      return 'You liked this.'
    }

    // crea un boton que al clickear cambia el estado
    // return e(
    //   'button',
    //   { onClick: () => this.setState({ liked: true }) },
    //   'Like'
    // );
    
    // JSX
    // return (
    //   <button onClick={() => this.setState({ liked: true })}>Like</button>
    // )
  }
}

const domContainer = document.querySelector('#like_button_container')
ReactDOM.render(e(LikeButton), domContainer)
